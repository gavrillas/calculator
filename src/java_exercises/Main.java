package java_exercises;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

import util.IntVector;

public class Main {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		LinkedList<Integer> realNumbers = new LinkedList<Integer>(); 
		while(sc.hasNextInt()){
			realNumbers.add(sc.nextInt());
		}
		int number = realNumbers.remove(realNumbers.size()-1);
		
		
		IntVector calculator = new IntVector(realNumbers);
		calculator.add(number);
		System.out.println(calculator.toString());
	}
}
