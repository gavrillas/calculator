package util;

import java.util.LinkedList;

public class IntVector {
	private LinkedList<Integer> numbers;
	
	public IntVector(LinkedList<Integer> numbers) {
		this.numbers = numbers; 
	}
	
	public void add(int number) {
		for (int i = 0; i < numbers.size(); i++) {
			numbers.set(i, numbers.get(i) + number);
		}
	}
	
	public String toString() {
		String string = "[";
		int count = 0;
		for (int i = 0; i < numbers.size(); i++) {
			if(i != 0) {
				string += " ";
			}
			string +=  numbers.get(i);
		}
		string += "]";
		return string;
	}

	public LinkedList<Integer> getNumbers() {
		return numbers;
	}

	public void setNumbers(LinkedList<Integer> numbers) {
		this.numbers = numbers;
	}


	
	
}
