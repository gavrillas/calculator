# Calculator

1. Készítsünk a util csomagon belül egy IntVector osztályt, mely egészek sorozatát ábrázolja!

    a. Legyen egy tömb adattag, mely a sorozatot tárolja. Tegyük rejtetté!

    b. Adjunk az osztályhoz egy konstruktort, mely egy egészekből álló tömböt vár paraméterül! Ügyeljünk, hogy a belső állapotot ne szivárogtassuk ki!

    c. Vegyünk fel egy add() metódust, mely a sorozat minden eleméhez hozzáad egy paraméterül kapott egész számot!

    d. Írjunk egy toString() metódust is, mely felsorolja a számokat szóközzel elválasztva. Például: [1 2 3]

2. Készítsünk egy Calculator programot, mely egy számsorozatot és egy számot kap argumentumként. A program a számsorozat minden eleméhez hozzáadja a második argumentumot. Például:

    $ java Calculator 1,2,3 5
    
    [6 7 8]
    
    Használjuk az előbb megírt util.IntVector osztályt!

3. Alakítsuk át az util.IntVector osztályt, hogy tömb helyett java.util.LinkedList<A> láncolt listában tárolja a számsorozatot!

    a. A lista végéhez hozzáadni az add(elem) metódussal lehet.
    
    b. A lista egy elemét a get(i) metódussal érjük el.
    
    c. A lista egy elemét a set(i, elem) metódussal írjuk felül.
    
    d. A lista hosszát a size() metódussal kapjuk meg.

4. Egyszerűsítsük le a Calculator programot a java.util.Scanner osztállyal!

    A java.util.Scanner legfontosabb műveletei a nextInt() és a hasNextInt().
